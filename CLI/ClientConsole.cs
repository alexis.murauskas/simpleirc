﻿using System;
using System.Threading.Tasks;
using SimpleIRC.ClientAPI;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;
using SimpleIRC.TcpConnection;

namespace SimpleIRC.CLI
{
    public class ClientConsole : IConsole
    {
        private readonly ClientConnection Connection;
        private readonly ClientController Client;
        private ConsoleColor ClientColor = ConsoleColor.Blue;


        public ClientConsole(ClientConnection connection, ClientController client)
        {
            Connection = connection;
            Client = client;
        }


        public async Task<Continue> Run(string input)
        {
            // Client input
            Console.ForegroundColor = ClientColor;


            try
            {
                Console.WriteLine("> " + input);
                var userMessage = MessageParser.ParseUserMessage(input);
                Connection.Write(userMessage);
            }
            catch (Exception e)
            {

            }

            try {
                var serverMessage = Connection.Read();
                if (serverMessage != null)
                {
                    Post(serverMessage);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("The client encountered error {0} and needs to close\n", e);
                return Continue.NO;
            }

            return Continue.YES;
        }


        public void Dispose()
        {
            Connection.Disconnect();
        }


        public void Post(IMessage message)
        {

        }
    }
}
