﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SimpleIRC.Connection;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;
using SimpleIRC.ServerAPI;

namespace SimpleIRC.CLI
{
    public class ServerConsole : IConsole
    {
        private readonly ServerConnection Connection;
        private readonly ServerController Server;
        private ConsoleColor AdminColor = ConsoleColor.Red;

        public ServerConsole(ServerConnection connection, ServerController server)
        {
            Connection = connection;
            Server = server;
        }


        public async Task<Continue> Run(string input)
        {
            // Admin input
            Console.ForegroundColor = AdminColor;

            try
            {
                Console.WriteLine("> " + input);
                var adminMessage = MessageParser.ParseAdminMessage(input);
                ProcessAdminCmd(adminMessage);

                if (adminMessage.AdminCommand == AdminCommand.QUIT)
                    return Continue.NO;
            }
            catch (Exception e)
            {
                Console.WriteLine("Admin command could not be parsed: {0}", e);
                return Continue.YES;
            }


            try
            {
                await Connection.CheckForPendingClients();
                var clientMessages = Connection.Read();

                if (clientMessages.Any())
                {
                    foreach (var m in clientMessages)
                        ProcessUserCmd(m);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Unable to process clients: {0}", e);
            }

            return Continue.YES;
        }


        public void Dispose()
        {
            Connection.Disconnect();
        }


        public void ProcessAdminCmd(AdminMessage message)
        {
            if(message.AdminCommand == AdminCommand.START)
            {
                Console.WriteLine("Server listening for incoming messages...");
            }

            if (message.AdminCommand == AdminCommand.QUIT)
            {
                Console.WriteLine("Server shutting down...");
                var response = Server.Disconnect();
                Connection.Write(response);
            }

            if(message.AdminCommand == AdminCommand.CREATE)
            {
                var response = Server.CreateChannel(message);
                Connection.Write(response);
            }

        }


        public void ProcessUserCmd(ConnectionFrame frame)
        {
            ServerMessage response = null;
            var message = frame.Message;

            switch (message.UserCommand) 
            {
                case UserCommand.CONNECT:
                    response = Server.Connect(message);
                    Connection.UpdateClientName(frame);
                    break;

                case UserCommand.JOIN:
                    response = Server.SubscribeUser(message);
                    break;

                case UserCommand.PART:
                    response = Server.UnsubscribeUser(message);
                    break;

                case UserCommand.QUIT:
                    response = Server.Disconnect(message);
                    Connection.Write(response);
                    Connection.DisconnectClient(frame);
                    return;

                case UserCommand.TOPICS:
                    response = Server.GetTopics(message);
                    break;

                case UserCommand.NAMES:
                    response = Server.GetChannelClients(message);
                    break;

                case UserCommand.MSG:
                    response = Server.ProcessUserPost(message);
                    break;
            }

            Connection.Write(response);
        }
    }
}
