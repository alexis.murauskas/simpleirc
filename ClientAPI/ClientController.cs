﻿using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ClientAPI
{
    public class ClientController : IChatController
    {
        private readonly IClient ChatClient;


        public ClientController(IClient chatClient)
        {
            ChatClient = chatClient;
        }


        public string GetTopic()
        {
            return ChatClient.GetTopic();
        }


        public string GetMessages()
        {
            return ChatClient.GetMessages();
        }


        public string PostMessage(IMessage message)
        {
            if (message == null)
                throw new System.ArgumentNullException(nameof(message));

            return ChatClient.UpdateChannelMessages(message);
        }


        public string JoinChannel(string topic)
        {
            if (topic == null)
                throw new System.ArgumentNullException(nameof(topic));

            return ChatClient.UpdateChannel(new ClientChannel(topic));
        }


        public string LeaveChannel()
        {
            return ChatClient.UpdateChannel(new ClientChannel(null));
        }
    }
}
