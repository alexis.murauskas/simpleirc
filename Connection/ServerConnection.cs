﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;
using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;
using SimpleIRC.TcpConnection;

namespace SimpleIRC.Connection
{
    public class ServerConnection : IConnection
    {
        private readonly TcpListener Listener;
        private readonly List<ClientConnection> Clients;


        public ServerConnection(TcpListener listener)
        {
            Listener = listener;
            Clients = new List<ClientConnection>();
            Listener.Start();
        }


        public string generateID() => Guid.NewGuid().ToString("X");


        public void Disconnect()
        {
            foreach (var c in Clients)
                c.Disconnect();

            Listener.Stop();
        }


        public void DisconnectClient(ConnectionFrame frame)
        {
            var connection = Clients.Find(c => c.ID == frame.ID);
            connection.Disconnect();
            Clients.Remove(connection);
        }


        public void UpdateClientName(ConnectionFrame frame)
        {
            var name = frame.Message.Source;
            Clients.Find(c => c.ID == frame.ID).UserName = new ClientName(name.Name, name.Nickname);
        }


        public async Task CheckForPendingClients()
        {
            while (Listener.Pending())
            {
                var client = await Listener.AcceptTcpClientAsync();
                Clients.Add(new ClientConnection(client, generateID()));
            }
        }


        public IEnumerable<ConnectionFrame> Read()
        {
            var result = new List<ConnectionFrame>();

            foreach(var c in Clients)
            {
                var data = (UserMessage)c.Read();
                if (data != null)
                    result.Add(new ConnectionFrame(c.ID, data));
            }

            return result;
        }


        public void Write(ServerMessage message)
        {
            foreach (var r in message.Recipients)
                Clients.Find(c => c.UserName?.Name == r.Name).Write(message);
        }
    }
}
