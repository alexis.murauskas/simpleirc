﻿using System.Net.Sockets;
using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;

namespace SimpleIRC.TcpConnection
{
    public class ClientConnection : IConnection
    {
        public const int BufferSize = 256;

        public readonly string ID;
        public ClientName UserName;

        private readonly TcpClient Client;
        private readonly NetworkStream Stream;


        public ClientConnection(TcpClient client, string id)
        {
            ID = id;
            UserName = null;
            Client = client;
            Stream = Client.GetStream();
        }


        public void Disconnect()
        {
            Stream.Close();
            Client.Close();
        }


        public void Write(IMessage message)
        {
            var data = System.Text.Encoding.ASCII.GetBytes(message.Stringify());
            Stream.Write(data, 0, data.Length);
        }


        public IMessage Read()
        {
            var data = new byte[BufferSize];
            var bytes = Stream.Read(data, 0, data.Length);
            var output = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

            if (MessageParser.IsFromClient(output))
                return MessageParser.ParseUserMessage(output);

            return MessageParser.ParseServerMessage(output);
        }
    }
}
