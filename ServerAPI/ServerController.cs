﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;

namespace SimpleIRC.ServerAPI
{
    public class ServerController : IChatController
    {
        private readonly IServer ChatServer;


        public ServerController (IServer chatServer)
        {
            ChatServer = chatServer;
        }


        // Admin

        public ServerMessage Disconnect()
        {
            return new ServerMessage
            (
                ChatServer.ServerName,
                ChatServer.Clients,
                ServerReply.RPL_DISCONNECT,
                UserCommand.QUIT,
                ServerPost.Disconnect(ChatServer.ServerName.Name)
            );
        }


        public ServerMessage CreateChannel(AdminMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var param = message.Parameters.ToList();
            var topic = param[(int)Param.TARGET];
            ChatServer.AddChannel(topic);

            return new ServerMessage
            (
                ChatServer.ServerName,
                ChatServer.Clients,
                ServerReply.RPL_CONFIRM,
                null,
                ServerPost.ChannelCreated(topic)
            );
        }


        // Client

        public ServerMessage Connect(UserMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            ChatServer.AddClient(message.ClientName);
            var recipient = new List<IName> { message.ClientName };

            return new ServerMessage
            (
                ChatServer.ServerName,
                recipient,
                ServerReply.RPL_CONFIRM,
                UserCommand.TOPICS,
                ServerPost.Connect(message.ClientName.Name)
            );
        }


        public ServerMessage Disconnect(UserMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            ChatServer.DisconnectClient(message.ClientName);
            var recipient = new List<IName> { message.ClientName };

            return new ServerMessage
            (
                ChatServer.ServerName,
                recipient,
                ServerReply.RPL_DISCONNECT,
                null,
                ServerPost.Disconnect(message.ClientName.Name)
            );
        }


        public ServerMessage SubscribeUser(UserMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var param = message.Parameters.ToList();
            var topic = param[(int)Param.TARGET];
            ChatServer.SubscribeClient(topic, message.ClientName);

            return new ServerMessage
            (
                ChatServer.ServerName,
                ChatServer.GetMembers(topic),
                ServerReply.RPL_NAMES,
                null,
                ServerPost.ClientJoined(message.ClientName.Nickname, topic)
            );
        }


        public ServerMessage UnsubscribeUser(UserMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var param = message.Parameters.ToList();
            var topic = param[(int)Param.TARGET];
            var response = ChatServer.UnsubscribeClient(topic, message.ClientName);

            return new ServerMessage
            (
                ChatServer.ServerName,
                ChatServer.GetMembers(topic),
                ServerReply.RPL_CONFIRM,
                null,
                ServerPost.ClientLeft(message.ClientName.Nickname, topic)
            );
        }


        public ServerMessage GetChannelClients(UserMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var recipient = new List<IName> { message.ClientName };
            var param = message.Parameters.ToList();
            var topic = param[(int)Param.TARGET];

            return new ServerMessage
            (
                ChatServer.ServerName,
                recipient,
                ServerReply.RPL_TOPICS,
                null,
                ServerPost.Clients(ChatServer.StringifyClients(topic))
            );
        }


        public ServerMessage GetTopics(UserMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var recipient = new List<IName> { message.ClientName };
            return new ServerMessage
            (
                ChatServer.ServerName,
                recipient,
                ServerReply.RPL_TOPICS,
                null,
                ServerPost.Topics(ChatServer.StringifyChannels())
            );
        }


        public ServerMessage ProcessUserPost(UserMessage message)
        {
            var param = message.Parameters.ToList();
            var topic = param[(int)Param.TARGET];
            var post = param[(int)Param.POST];

            return new ServerMessage
            (
                ChatServer.ServerName,
                ChatServer.GetMembers(topic),
                ServerReply.RPL_MSG,
                null,
                post
            );
        }
    }
}
