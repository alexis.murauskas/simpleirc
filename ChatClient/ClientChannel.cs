﻿using System.Collections.Generic;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatClient
{
    public class ClientChannel : IChannel
    {
        public readonly string Topic;
        public List<IMessage> Messages { get; set; }

        public ClientChannel(string topic)
        {
            Topic = topic;
            Messages = new List<IMessage>();
        }

        string IChannel.Topic => Topic;

        public string StringifyMessages()
        {
            var rv = "";
            foreach (var m in Messages)
                rv += m.Stringify() + '\n';

            return rv;
        }
    }
}
