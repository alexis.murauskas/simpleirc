﻿using System;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatClient
{
    public class Client : IClient
    {
        public readonly ClientName Name;
        private ClientChannel Channel;


        public Client(ClientName name)
        {
            Name = name;
        }


        public string GetTopic()
        {
            return Channel.Topic;
        }


        public string GetMessages()
        {
            return Channel.StringifyMessages();
        }


        public string UpdateChannel(ClientChannel channel)
        {
            Channel = channel ?? throw new ArgumentNullException(nameof(channel));
            return Channel.Topic;
        }


        public string UpdateChannelMessages(IMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            Channel.Messages.Add(message);
            return Channel.StringifyMessages();
        }
    }
}
