﻿using System.Collections.Generic;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatServer
{
    public class ServerChannel : IChannel
    {
        public readonly string Topic;
        public List<IName> Members { get; set; }

        public ServerChannel(string topic)
        {
            Topic = topic;
            Members = new List<IName>();
        }

        string IChannel.Topic => Topic;
    }
}
