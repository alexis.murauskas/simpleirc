﻿using System;
namespace SimpleIRC.Interfaces
{
    public interface IName
    {
        string Name { get; }
        string Nickname { get; }
    }
}
