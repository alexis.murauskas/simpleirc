﻿using System.Collections.Generic;
using SimpleIRC.ChatClient;
using SimpleIRC.ChatServer;

namespace SimpleIRC.Interfaces
{
    public interface IServer
    {
        ServerName ServerName { get; }
        List<IName> Clients { get; }

        IEnumerable<string> StringifyChannels();
        IEnumerable<string> StringifyClients(string topic);
        IEnumerable<IName> GetMembers(string topic);

        IEnumerable<string> AddClient(ClientName client);
        IEnumerable<string> AddChannel(string topic);

        IEnumerable<string> SubscribeClient(string topic, ClientName client);
        IEnumerable<string> UnsubscribeClient(string topic, ClientName client);
        IEnumerable<string> DisconnectClient(ClientName client);
    }
}
