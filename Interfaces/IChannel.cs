﻿using System.Collections.Generic;

namespace SimpleIRC.Interfaces
{
    public interface IChannel
    {
        string Topic { get; }
    }
}
