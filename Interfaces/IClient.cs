﻿using System.Threading.Tasks;
using SimpleIRC.ChatClient;

namespace SimpleIRC.Interfaces
{
    public interface IClient
    {
        string GetTopic();
        string GetMessages();
        string UpdateChannel(ClientChannel channel);
        string UpdateChannelMessages(IMessage message);
    }
}
