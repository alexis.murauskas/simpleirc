﻿using SimpleIRC.CLI;
using System;
using System.Threading.Tasks;

namespace SimpleIRC.Interfaces
{
    public interface IConsole
    {
        Task<Continue> Run(string input);
    }
}
