﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using SimpleIRC.ChatClient;
using SimpleIRC.ChatServer;
using SimpleIRC.CLI;
using SimpleIRC.ClientAPI;
using SimpleIRC.Connection;
using SimpleIRC.ServerAPI;
using SimpleIRC.TcpConnection;

namespace SimpleIRC
{
    public class Program
    {
        public static int Port = 13000;
        public static IPAddress Ip;

        public static async Task Main(string[] args)
        {
            var addr = GetLocalIPAddress();
            Ip = IPAddress.Parse(addr);

            // DI for server
            var listener = new TcpListener(Ip, Port);
            var serverConnection = new ServerConnection(listener);
            var server = new Server(new ServerName(addr));
            var serverController = new ServerController(server);
            var serverConsole = new ServerConsole(serverConnection, serverController);

            await serverConsole.Run(addr + " /start");

            // DI for client
            var tcpClient = new TcpClient(addr, Port);
            var clientConnection = new ClientConnection(tcpClient, "");
            clientConnection.UserName = new ClientName(addr, "Lula");
            var client = new Client(clientConnection.UserName);
            var clientController = new ClientController(client);
            var clientConsole = new ClientConsole(clientConnection, clientController);

            // await serverConsole.Run(addr + " /create room");
            await serverConsole.Run(addr + " /quit");

            clientConsole.Dispose();
            serverConsole.Dispose();
        }


        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine("Local IPv4 Address: " + ip.ToString());
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
