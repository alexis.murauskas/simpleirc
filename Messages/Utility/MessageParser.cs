﻿using System;
using SimpleIRC.ChatServer;
using SimpleIRC.ChatClient;
using System.Collections.Generic;

namespace SimpleIRC.Messages
{
    public static class MessageParser
    {
        private static readonly string[] AdminCommands =
        {
            "/start",
            "/quit",
            "/create"
        };

        private static readonly string[] UserCommands = 
        {
            "/connect",
            "/join",
            "/part",
            "/quit",
            "/topics",
            "/names",
            "/msg"
        };

        private static readonly string[] ServerReplies =
        {
            "RPL_CONFIRM",
            "RPL_ERROR",
            "RPL_DISCONNECT",
            "RPL_MSG",
            "RPL_NAMES",
            "RPL_TOPICS"
        };


        // Field parsing

        public static bool IsFromClient(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ');
            if (list[(int)MsgProp.SOURCE].Contains(":"))
                return true;

            return false;
        }


        public static AdminCommand ParseAdminCommand(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var index = Array.IndexOf(AdminCommands, input);

            if (index < AdminCommands.GetLowerBound(0))
                throw new ArgumentException(nameof(input));

            return (AdminCommand)index;
        }


        public static UserCommand ParseUserCommand(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var index = Array.IndexOf(UserCommands, input);

            if (index < UserCommands.GetLowerBound(0))
                throw new ArgumentException(nameof(input));

            return (UserCommand)index;
        }


        public static ServerReply ParseServerReply(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var index = Array.IndexOf(ServerReplies, input);

            if (index < ServerReplies.GetLowerBound(0))
                throw new ArgumentException(nameof(input));

            return (ServerReply)index;
        }


        public static ClientName ParseClientName(string input)
        {
            var names = input.Split(':');
            return new ClientName(names[(int)Name.IP], names[(int)Name.NICK]);
        }


        // Message parsing

        public static AdminMessage ParseAdminMessage(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ');
            List<string> param = null;

            if (list.Length > (int)MsgProp.PARAMS)
                param = new List<string> { list[(int)MsgProp.PARAMS] };

            var message = new AdminMessage
            (
                new ServerName(list[(int)MsgProp.SOURCE]),
                ParseAdminCommand(list[(int)MsgProp.CMD]),
                param
            );

            return message;
        }


        public static UserMessage ParseUserMessage(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ');
            string[] param = null;
            list.CopyTo(param, (int)MsgProp.PARAMS);

            var message = new UserMessage
            (
                ParseClientName(list[(int)MsgProp.SOURCE]),
                ParseUserCommand(list[(int)MsgProp.CMD]),
                param
            );

            return message;
        }


        public static ServerMessage ParseServerMessage(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ');
            var recipients = list[(int)SrvMsgProp.TARGET].Split(',');
            var names = new List<ClientName>();
            UserCommand? cmd = null;

            foreach (var r in recipients)
                names.Add(ParseClientName(r));

            try { cmd = ParseUserCommand(list[(int)SrvMsgProp.CMD]);}
            catch {}

            var message = new ServerMessage
            (
                new ServerName(list[(int)SrvMsgProp.SOURCE]),
                names,
                ParseServerReply(list[(int)SrvMsgProp.REPLY]),
                cmd,
                list[(int)SrvMsgProp.MSG]
            );

            return message;
        }
    }
}
