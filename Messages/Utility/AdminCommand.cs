﻿namespace SimpleIRC.Messages
{
    public enum AdminCommand
    {
        START,
        QUIT,
        CREATE
    }
}
