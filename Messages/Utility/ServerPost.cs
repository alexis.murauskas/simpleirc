﻿using System.Collections.Generic;

namespace SimpleIRC.Messages
{
    public static class ServerPost
    {
        public static string Connect(string name) => name + "CONNECTED\n";
        public static string Disconnect(string name) => name + "DISCONNECTED\n";


        public static string Topics(IEnumerable<string> topics)
        {
            var rv = "TOPICS\n";
            foreach (var t in topics)
                rv += t + '\n';

            return rv;
        }


        public static string Clients(IEnumerable<string> clients)
        {
            var rv = "MEMBERS";
            foreach (var c in clients)
                rv += c + '\n';

            return rv;
        }


        public static string ClientJoined(string name, string channel)
            => name + "JOINED" + channel + '\n';


        public static string ClientLeft(string name, string channel)
            => name + "LEFT" + channel + '\n';


        public static string ChannelCreated(string channel) 
            => "CHANNEL" + channel + "CREATED" + '\n';
    }
}
